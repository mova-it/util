package mova.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Stream;

public class IOUtils {

    private IOUtils() {}

    public static String toString(InputStream input) throws IOException {
        return org.apache.commons.io.IOUtils.toString(input, (Charset) null);
    }

    public static String toString(InputStream input, String encoding) throws IOException {
        return org.apache.commons.io.IOUtils.toString(input, encoding);
    }

    public static String toString(InputStream input, Charset encoding) throws IOException {
        return org.apache.commons.io.IOUtils.toString(input, encoding);
    }

    public static Stream<String> lines(InputStream inputStream) {
        InputStreamReader isr = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(isr);
        return br.lines();
    }
}
