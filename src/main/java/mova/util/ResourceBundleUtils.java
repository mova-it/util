package mova.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class ResourceBundleUtils {

    private ResourceBundleUtils() {}

    public static String format(ResourceBundle ressourceBundle, String key, Object... args) {
        String pattern = ressourceBundle.getString(key);
        return args != null && args.length > 0 ? MessageFormat.format(pattern, args) : pattern;
    }
}
