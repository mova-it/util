package mova.util;

import mova.lang.EnumUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.function.Function;

public class Properties extends HashMap<String, String> {

    public Integer putInteger(String key, Integer i) {
        return putSafely(key, i, Object::toString, Integer::valueOf);
    }

    public Boolean putBoolean(String key, Boolean b) {
        return putSafely(key, b, Object::toString, Boolean::valueOf);
    }

    public <P> String put(String key, P value, Function<P, String> convertingFunction) {
        return super.put(key, convertingFunction.apply(value));
    }

    private <V, P> V putSafely(String key, P value, Function<P, String> convertingFunction, Function<String, V> parsingFunction) {
        String oldValue = put(key, value, convertingFunction);
        return parseSafely(oldValue, parsingFunction, null);
    }

    public Integer getInteger(Object key) {
        return get(key, Integer::valueOf);
    }

    public Integer getIntegerOrDefault(Object key, int defaultValue) {
        return getOrDefault(key, Integer::valueOf, defaultValue);
    }

    public int num(Object key) {
        return Integer.parseInt(super.get(key));
    }

    public boolean is(Object key) {
        return Boolean.parseBoolean(super.get(key));
    }

    public Boolean getBoolean(Object key) {
        return get(key, Boolean::valueOf);
    }

    public Boolean getBooleanOrDefault(Object key, boolean defaultValue) {
        return getOrDefault(key, Boolean::valueOf, defaultValue);
    }

    public <P> P get(Object key, Function<String, P> parsingFunction) {
        String sValue = super.get(key);
        return parseSafely(sValue, parsingFunction, null);
    }

    public <P> P getOrDefault(Object key, Function<String, P> parsingFunction, P defaultValue) {
        String sValue = super.get(key);
        return parseSafely(sValue, parsingFunction, defaultValue);
    }

    private <P> P parseSafely(String value, Function<String, P> parsingFunction, P defaultValue) {
        return value != null ? parsingFunction.apply(value) : defaultValue;
    }

    public <P> P get(Object key, Class<P> clazz) {
        String sValue = super.get(key);
        return parseSafely(sValue, s -> {
            if (clazz.isEnum()) {
                String normalizedValue = s.toUpperCase().replace('-', '_');

                Enum<?> o = EnumUtils.getEnum(clazz.asSubclass(Enum.class), normalizedValue);
                return clazz.cast(o);
            } else {
                try {
                    // On récupère un constructeur publique prenant en paramètre un String
                    Constructor<P> constructor = clazz.getConstructor(String.class);
                    // On renvoie la valeur typée à partir de ce constructeur
                    return constructor.newInstance(s);
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    throw new IllegalArgumentException("Wrong requested type.", e);
                }
            }
        }, null);
    }
}
