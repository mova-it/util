package mova.util;

import java.util.Arrays;

public class NumberUtils {

    private NumberUtils() {}

    public static long nthDigit(long num, int n) {
        return nthDigit(num, n, false);
    }

    public static long nthDigit(long num, int n, boolean trailingNumbers) {
        int divider = 1;
        while (n-- > 0) divider *= 10;

        return trailingNumbers ? num/divider : num/divider%10;
    }

    public static int[] convert(String[] strings) {
        return Arrays.stream(strings).mapToInt(Integer::parseInt).toArray();
    }
}
