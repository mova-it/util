package mova.util.function;

import java.util.Objects;
import java.util.function.Consumer;

public class FunctionalInterfaceUtils {

    private FunctionalInterfaceUtils() {}

    public static <T> Consumer<T> noopConsumer() {
        return o -> {};
    }

    public static Runnable noopRunnable() {
        return () -> {};
    }

    public static <T> Consumer<T> onlyOnce(Consumer<T> consumer) {
        return new OneTimeConsumer<>(consumer);
    }

    public static Runnable onlyOnce(Runnable runnable) {
        return new OneTimeRunnable(runnable);
    }

    private abstract static class AbstractOneTimeDoable implements Doable {

        protected boolean done = false;

        public boolean isDone() {
            return done;
        }
    }

    private static class OneTimeConsumer<T> extends AbstractOneTimeDoable implements Consumer<T> {

        private final Consumer<T> consumer;

        private OneTimeConsumer(Consumer<T> consumer) {
            Objects.requireNonNull(consumer);
            this.consumer = consumer;
        }

        @Override
        public final void accept(T t) {
            if (!isDone()) {
                consumer.accept(t);
                done = true;
            }
        }

        @Override
        public OneTimeConsumer<T> andThen(Consumer<? super T> after) {
            Objects.requireNonNull(after);
            return new OneTimeConsumer<>(consumer.andThen(after));
        }
    }

    private static class OneTimeRunnable extends AbstractOneTimeDoable implements Runnable {

        private final Runnable runnable;

        private OneTimeRunnable(Runnable runnable) {
            Objects.requireNonNull(runnable);
            this.runnable = runnable;
        }

        @Override
        public void run() {
            if (!isDone()) {
                runnable.run();
                done = true;
            }
        }
    }
}
