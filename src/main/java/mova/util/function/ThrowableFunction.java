package mova.util.function;

@FunctionalInterface
public interface ThrowableFunction<T, R> {

    @SuppressWarnings("RedundantThrows") // On veut que le code appelant gère l'exception
    R apply(T t) throws InApplicationException;
}
