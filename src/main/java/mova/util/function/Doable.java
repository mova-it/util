package mova.util.function;

public interface Doable {
    boolean isDone();
}
