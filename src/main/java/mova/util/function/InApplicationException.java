package mova.util.function;

public class InApplicationException extends Exception {

    public InApplicationException() {
    }

    public InApplicationException(String message) {
        super(message);
    }

    public InApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InApplicationException(Throwable cause) {
        super(cause);
    }

    public InApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
