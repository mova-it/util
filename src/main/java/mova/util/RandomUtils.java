package mova.util;

import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collector;

public class RandomUtils {

    private RandomUtils() {}

    @SafeVarargs
    public static <T> T randElement(T... elements) {
        return elements[randInt(elements.length)];
    }

    public static <T extends Enum<T>> T randElement(Class<T> enumType) {
        T[] items = enumType.getEnumConstants();
        return randElement(items);
    }

    @SuppressWarnings("unchecked")
    public static <E> E randElement(Collection<? extends E> elements) {
        Object[] arrays = elements.toArray();
        return (E) randElement(arrays);
    }

    public static String randString(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)(alea().nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        return buffer.toString();
    }

    public static String randString(int length, String... exclusions) {
        List<String> exclusionList = Arrays.asList(exclusions);

        String s;
        do s = RandomUtils.randString(length);
        while (exclusionList.contains(s));

        return s;
    }

    public static String randLengthString(char c, int maxLength) {
        char[] chars = new char[randInt(maxLength + 1)];
        Arrays.fill(chars, c);
        return new String(chars);
    }

    public static boolean randBool() {
        return alea().nextBoolean();
    }

    public static boolean tail() {
        return randBool();
    }

    public static int randInt(int bound) { return alea().nextInt(bound); }

    public static float randFloat() {
        return alea().nextFloat();
    }

    public static <E> Collection<E> randLengthElements(Class<E> enumType, int minLength, int maxLength, Collector<E, ?, Collection<E>> collector) {
        E[] elements = enumType.getEnumConstants();
        return randLengthElements(elements, enumType, minLength, maxLength, collector);
    }

    public static <C extends Collection<E>, E> C randLengthElements(Collection<E> elements, Class<E> type, int minLength, int maxLength, Collector<E, ?, C> collector) {
        @SuppressWarnings("unchecked")
        E[] items = (E[]) elements.toArray();
        return randLengthElements(items, type, minLength, maxLength, collector);
    }

    public static <C extends Collection<E>, E> C randLengthElements(E[] elements, Class<E> type, int minLength, int maxLength, Collector<E, ?, C> collector) {
        @SuppressWarnings("unchecked")
        E[] randElements = (E[]) Array.newInstance(type, minLength + alea().nextInt(maxLength - minLength));
        for (int i = 0; i < randElements.length; i++) randElements[i] = randElement(elements);

        return Arrays.stream(randElements).collect(collector);
    }

    public static <E> E[] shuffle(E[] elements) {
        ArrayUtils.shuffle(elements, alea());
        return elements;
    }

    @SuppressWarnings("java:S2245")
    private static ThreadLocalRandom alea() {
        return ThreadLocalRandom.current();
    }
}
