package mova.util;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;

public class ArrayUtils {

    private ArrayUtils() {}

    public static <T> T last(T[] ts) {
        if (ts == null || ts.length == 0) throw new IllegalArgumentException("Array can't be null or empty.");

        return ts[ts.length -1];
    }

    public static boolean isEmpty(final Object[] array) {
        return array.length == 0;
    }

    public static <T> T[] invert(T[] ts) {
        return IntStream.rangeClosed(1, ts.length)
                .mapToObj(i -> ts[ts.length - i])
                .toArray(value -> newInstance(getArrayType(ts), value));
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getArrayType(T[] ts) {
        return (Class<T>) ts.getClass().getComponentType();
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] newInstance(Class<T> type, int length) {
        return (T[]) Array.newInstance(type, length);
    }

    public static int indexOf(Object[] ts, Object t) {
        return Arrays.asList(ts).indexOf(t);
    }

    public static void swap(Object[] array, int i, int j) {
        Object element = array[j];
        array[j] = array[i];
        array[i] = element;
    }

    public static String printSquareFormat(int[] ints, int w, int h, int intLength) {
        // on ajoute un espace
        String spaceFormat = "%" + (intLength + 1) + "d";
        String emptyFormat = "%" + (intLength + 1) + "s";

        String[] lines = new String[h];
        for (int y = 0; y < h; y++) {
            StringBuilder builder = new StringBuilder();

            builder.append("[");

            IntStream.range(y*w, y*w + w).forEach(i -> builder.append(i < ints.length ? String.format(spaceFormat, ints[i]) : String.format(emptyFormat, "")));

            builder.append(" ]");

            lines[y] = builder.toString();
        }

        return String.join("\n", lines);
    }
}
