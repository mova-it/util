package mova.util;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("java:S2160") // On respecte le principe d'égalité lié au List
public class BoundList<E> extends ArrayList<E> {

    private final int bound;

    public BoundList(int bound) {
        super(bound);

        this.bound = bound;
    }

    public BoundList(Collection<? extends E> c) {
        this(c, c.size());
    }

    public BoundList(Collection<? extends E> c, int bound) {
        super(c);

        if (bound < c.size()) throw new IllegalArgumentException("Bound can't be lower than parameter collection size.");

        this.bound = bound;
        trimToSize();
    }

    @Override
    public boolean add(E e) {
        if (size() + 1 > bound) throw new IndexOutOfBoundsException();
        boolean changed = super.add(e);
        trimToSize();

        return changed;
    }

    @Override
    public void add(int index, E e) {
        if (size() + 1 > bound) throw new IndexOutOfBoundsException();
        super.add(index, e);
        trimToSize();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (size() + c.size() > bound) throw new IndexOutOfBoundsException();
        boolean changed =  super.addAll(c);
        trimToSize();

        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (size() + c.size() > bound) throw new IndexOutOfBoundsException();
        boolean changed = super.addAll(index, c);
        trimToSize();

        return changed;
    }

    @Override
    public void ensureCapacity(int minCapacity) {
        throw new UnsupportedOperationException("Maximum size of this list is immutable.");
    }

    public int getBound() {
        return bound;
    }

    public boolean isFull() {
        return size() == bound;
    }

}
