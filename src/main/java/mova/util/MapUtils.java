package mova.util;

import java.util.Map;
import java.util.stream.Collectors;

public class MapUtils {

    private MapUtils() {}

    public static boolean isEmpty(Map<?, ?> map) {
        return org.apache.commons.collections4.MapUtils.isEmpty(map);
    }

    public static <K, T> Map<K, T> mapValuesTo(Map<K, ?> map, Class<T> toClass) {
        return map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> toClass.cast(entry.getValue())));
    }

}
