package mova.util.logging;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerUtils {

    public static final String ROOT_LOGGER_NAME = "";

    private LoggerUtils() {}

    public static void setRootConsoleLog(Level level) {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(level);
        Logger.getLogger(ROOT_LOGGER_NAME).addHandler(handler);
        Logger.getLogger(ROOT_LOGGER_NAME).setLevel(level);
    }
}
