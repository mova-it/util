package mova.util;

import java.util.Arrays;
import java.util.Collection;

public class StringUtils {

    public static final String EMPTY = org.apache.commons.lang3.StringUtils.EMPTY;
    public static final String SPACE = org.apache.commons.lang3.StringUtils.SPACE;

    private StringUtils() {}

    public static String deleteWhitespace(String s) {
        return org.apache.commons.lang3.StringUtils.deleteWhitespace(s);
    }

    public static boolean isNotBlank(CharSequence s) {
        return org.apache.commons.lang3.StringUtils.isNotBlank(s);
    }

    public static boolean isBlank(CharSequence s) {
        return org.apache.commons.lang3.StringUtils.isBlank(s);
    }

    public static String peel(String s) {
        return peel(s, 1);
    }

    public static String peel(String s, int nbChars) {
        return s.substring(nbChars, s.length() - nbChars);
    }

    public static String join(Collection<?> collection) {
        return join(collection, ",");
    }

    public static String join(Collection<?> collection, String delimiter) {
        return org.apache.commons.lang3.StringUtils.join(collection, delimiter);
    }

    public static String capitalize(String s) {
        return org.apache.commons.lang3.StringUtils.capitalize(s);
    }

    public static String uncapitalize(String s) {
        return org.apache.commons.lang3.StringUtils.uncapitalize(s);
    }

    public static String fill(char c, int length) {
        char[] chars = new char[length];
        Arrays.fill(chars, c);
        return new String(chars);
    }

}
