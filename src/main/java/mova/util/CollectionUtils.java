package mova.util;

import java.util.*;

public class CollectionUtils {

    private CollectionUtils() {}

    public static <E> E first(List<? extends E> list) {
        if (isEmpty(list)) throw new IllegalArgumentException("List can't be null or empty.");

        return list.get(0);
    }

    public static <E> E last(List<? extends E> list) {
        if (isEmpty(list)) throw new IllegalArgumentException("List can't be null or empty.");

        return list.get(list.size() - 1);
    }

    public static boolean isEmpty(Collection<?> collection) {
        return org.apache.commons.collections4.CollectionUtils.isEmpty(collection);
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return org.apache.commons.collections4.CollectionUtils.isNotEmpty(collection);
    }

    @SafeVarargs
    public static <E> Set<E> asSet(E... es) {
        return new HashSet<>(Arrays.asList(es));
    }

    public static <E> List<E> substract(List<E> c1, List<E> c2) {
        ArrayList<E> newList = new ArrayList<>(c1);
        for (E aListToSubstract : c2) newList.remove(aListToSubstract); // pas de removeAll car ce dernier supprime TOUS les itérations d'un élément et non juste la première occurence
        return newList;
    }

    public static <E> void move(List<E> list, int from, int to) {
        list.add(to, list.remove(from));
    }
}
