package mova.util.regex;

import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.regex.Matcher;

public class MatcherSpliterator extends Spliterators.AbstractSpliterator<String> {

    private final Matcher matcher;

    public MatcherSpliterator(Matcher matcher) {
        // On cast to long pour respecter les standards SonarLint
        super(matcher.regionEnd() - ((long) matcher.regionStart()), ORDERED | NONNULL);
        this.matcher = matcher;
    }

    public boolean tryAdvance(Consumer<? super String> action) {
        if (!matcher.find()) return false;

        action.accept(matcher.group());
        return true;
    }
}
