package mova.util.test;

import mova.util.CollectionUtils;
import mova.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class AssertUtils {

    private AssertUtils() {}

    public static void hasText(String text, String message) {
        if (StringUtils.isBlank(text)) throw new IllegalArgumentException(message);
    }

    public static void notEmpty(Collection<?> collection, String message) {
        if (CollectionUtils.isEmpty(collection)) throw new IllegalArgumentException(message);
    }

    public static <E> void noneMatch(Collection<? extends E> collection, Predicate<E> predicate, String message) {
        if (collection.stream().anyMatch(predicate)) {
            List<E> list = collection.stream().filter(predicate).collect(Collectors.toList());
            throw new IllegalArgumentException(message + " " + list);
        }
    }

    public static void assertInRangeClosed(int min, int max, int result) {
        assertTrue(result >= min, result + " < " + min);
        assertTrue(result <= max, result + " > " + max);
    }

    public static void notNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isTrue(boolean expression, String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isFalse(boolean expression, String message) {
        isTrue(!expression, message);
    }
}
