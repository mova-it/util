package mova.util;

import java.util.*;

/**
 * Une classe qui compte le nombre d'occurence d'un objet en les comparants sur la base de leur méthode equals.
 * <p/>
 * Le compte est fait à la création du compteur, il n'y a donc pas de rétroaction si la source du comptage est modifiée.
 *
 * @param <E> Le type de l'élément compté
 */
public class Counter<E> {

    private final Map<E, Integer> map = new HashMap<>();

    public Counter(E[] elements) {
        this(Arrays.asList(elements));
    }

    public Counter(List<E> elements) {
        for (E e : elements) {
            // On initialise le compteur à 1, sinon on l'incrémente
            map.merge(e, 1, Integer::sum);
        }
    }

    /**
     * Renvoie le nombre d'occurence de l'objet, -1 sinon.
     *
     * @param e l'objet dont on cherche le nombre
     * @return le nombre d'occurence
     */
    public int get(E e) {
        return map.getOrDefault(e, -1);
    }

    public Collection<E> references() {
        return map.keySet();
    }

    @Override
    public String toString() {
        return map.toString();
    }
}
