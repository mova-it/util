package mova.trigo;

import java.awt.geom.Point2D;

public class TrigoUtils {

    private TrigoUtils() {}

    public static final double PI_SUR_SIX = Math.PI/6;
    public static final double CINQ_PI_SUR_SIX = 5*PI_SUR_SIX;
    public static final double PI_SUR_QUATRE = Math.PI/4;
    public static final double TROIS_PI_SUR_QUATRE = 3*PI_SUR_QUATRE;
    public static final double PI_SUR_TROIS = Math.PI/3;
    public static final double DEUX_PI_SUR_TROIS = 2*PI_SUR_TROIS;
    public static final double PI_SUR_DEUX = Math.PI/2;
    public static final double PI = Math.PI;
    public static final double DEUX_PI = Math.PI*2;

    public static double substractAtan2(double y1, double x1, double y2, double x2) {
        // équivaut à : Math.atan2(y1, x1) - Math.atan2(y2, x2)
        return Math.atan2(y1*x2 - y2*x1, x1*x2 + y1*y2);
    }

    public static double sumAtan2(double y1, double x1, double y2, double x2) {
        // équivaut à : Math.atan2(y1, x1) + Math.atan2(y2, x2)
        return Math.atan2(y1*x2 + y2*x1, x1*x2 - y1*y2);
    }

    public static boolean isOutOfBounds(double teta, double minPhi, double maxPhi) {
        if (maxPhi < minPhi) return teta < minPhi && teta > maxPhi;
        else return teta > maxPhi || teta < minPhi;
    }

    public static double normalize(double teta) {
        return normalize(teta, -Math.PI, Math.PI);
    }

    public static double normalize(double teta, double min, double max) {
        while (teta > max) teta -= DEUX_PI;
        while (teta < min) teta += DEUX_PI;

        return teta;
    }

    public static double atan2(Point2D from, Point2D to) {
        return Math.atan2(to.getY() - from.getY(), to.getX() - from.getX());
    }

    public static int quadrant(double angle) {
        angle = normalize(angle);
        double absoluteAngle = Math.abs(angle);
        if (absoluteAngle == PI || absoluteAngle == PI_SUR_DEUX || absoluteAngle == 0) return 0;

        if (angle < -PI_SUR_DEUX) return 3;
        if (angle < 0) return 4;
        if (angle > PI_SUR_DEUX) return 2;
        return 1;
    }

    public static int quadrantFrom(Point2D p, Point2D from) {
        int x = (int) (p.getX() - from.getX());
        if (x == 0) return 0;
        int y = (int) (p.getY() - from.getY());
        if (y == 0) return 0;

        y = y >>> 31;
        return ((x >>> 31) ^ y) + y + y + 1;
    }

    public static float convert(float cosOrSin, float sign) {
        return Math.copySign((float) Math.sqrt(1 - cosOrSin * cosOrSin), sign);
    }
}
