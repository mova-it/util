package mova.trigo;

import java.util.Arrays;

public class Angle implements Comparable<Angle> {
    
    private final float[] values = new float[2];

    public Angle() {
        set(1, 0);
    }

    public Angle(float radians) {
        set(radians);
    }

    public Angle(Angle angle) {
        set(angle);
    }
    
    public Angle(float cos, float sin) {
        set(cos, sin);
    }

    public float cos() {
        return values[0];
    }

    public float sin() {
        return values[1];
    }

    public float toRadians() {
        return (float) Math.atan2(values[1], values[0]);
    }

    public float toDegrees() {
        return (float) Math.toDegrees(toRadians());
    }

    public void set(float radians) {
        set((float) Math.cos(radians), (float) Math.sin(radians));
    }

    public void set(Angle angle) {
        set(angle.cos(), angle.sin());
    }

    public void set(float[] values) {
        set(values[0], values[1]);
    }

    public void set(float cos, float sin) {
        values[0] = cos;
        values[1] = sin;
    }
    
    public void add(float radians) {
        add((float) Math.cos(radians), (float) Math.sin(radians));
    }
    
    public void add(Angle angle) {
        add(angle.cos(), angle.sin());
    }
    
    public void add(float[] values) {
        add(values[0], values[1]);
    }
    
    public void add(float cos, float sin) {
        float newCos = values[0] * cos - values[1] * sin;
        float newSin = values[1] * cos + values[0] * sin;

        values[0] = newCos;
        values[1] = newSin;
    }

    public void sub(float radians) {
        sub((float) Math.cos(radians), (float) Math.sin(radians));
    }

    public void sub(Angle angle) {
        sub(angle.cos(), angle.sin());
    }

    public void sub(float[] values) {
        sub(values[0], values[1]);
    }

    public void sub(float cos, float sin) {
        add(cos, -sin);
    }

    public void capCos(float min, float max) {
        // Note: cos²(x) + sin²(x) = 1
        if (values[0] < min) {
            values[0] = min;
            values[1] = TrigoUtils.convert(min, values[1]);
        } else if (values[0] > max) {
            values[0] = max;
            values[1] = TrigoUtils.convert(max, values[1]);
        }
    }

    public void capSin(float min, float max) {
        // Note: cos²(x) + sin²(x) = 1
        if (values[1] < min) {
            values[1] = min;
            values[0] = TrigoUtils.convert(min, values[0]);
        } else if (values[1] > max) {
            values[1] = max;
            values[0] = TrigoUtils.convert(max, values[0]);
        }
    }

    // La comparaison se fait sur la base d'angle normalisé [-¶, ¶]
    @Override
    public int compareTo(Angle other) {
        float sinSignum = Math.signum(values[1]);
        float otherSinSignum = Math.signum(other.values[1]);

        // Cas this negatif et other positif
        if (sinSignum < otherSinSignum) {
            return -1;
        }

        // Cas this positif et other negatif
        if (sinSignum > otherSinSignum) {
            return 1;
        }

        return (int) (sinSignum*Float.compare(other.values[0], values[0]));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Angle angle = (Angle) o;

        return equals(angle);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    public boolean equals(float radians) {
        return equals((float) Math.cos(radians), (float) Math.sin(radians));
    }

    public boolean equals(Angle angle) {
        return equals(angle.cos(), angle.sin());
    }

    public boolean equals(float cos, float sin) {
        return values[0] == cos && values[1] == sin;
    }

    public static Angle fromDegrees(float degrees) {
        return new Angle((float) Math.toRadians(degrees));
    }

}
