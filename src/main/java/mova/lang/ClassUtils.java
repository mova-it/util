package mova.lang;

import java.lang.reflect.Constructor;

public class ClassUtils {

    private ClassUtils() {}

    public static Class<?> forName(String name) throws ClassNotFoundException {
        return org.apache.commons.lang3.ClassUtils.getClass(name);
    }

    public static Object newInstance(String className) throws ReflectiveOperationException {
        Class<?> type = forName(className);
        Constructor<?> constructor = type.getConstructor();
        return constructor.newInstance();
    }
}
