package mova.lang;

import mova.util.StringUtils;

public class ByteUtils {

    private static final String HEX_FORMAT = "0x%02x";

    private ByteUtils() {}

    public static String toHexString(byte[] bytes) {
        String[] strings = new String[bytes.length];
        for (int i = 0, bytecodeLength = bytes.length; i < bytecodeLength; i++) {
            strings[i] = String.format(HEX_FORMAT, bytes[i]);
        }
        return String.join(StringUtils.SPACE, strings);
    }
}
