package mova.lang;

public class EnumUtils {

    private EnumUtils() {}

    public static <E extends Enum<E>> E getEnum(final Class<E> enumClass, final String enumName) {
        return Enum.valueOf(enumClass, enumName);
    }

}
