package mova.lang;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadUtils {

    private ThreadUtils() {}

    public static void lazilyExit() {
        Thread thread = new Thread(() -> {
            // first, wait for the VM exit on its own.
            takeANap(2000);
            // system is still running, so force an exit
            System.exit(0);
        });

        thread.setDaemon(true);
        thread.start();
    }

    public static void takeANap(long millis) {
        Thread currentThread = Thread.currentThread();
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadUtils.class.getName()).log(Level.FINE, "Lazily exit: Interruption du thread {0} [{1}]", new Object[]{currentThread.getName(), currentThread.getId()});
            Thread.currentThread().interrupt();
        }
    }
}
