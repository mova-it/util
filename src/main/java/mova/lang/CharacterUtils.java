package mova.lang;

public class CharacterUtils {

    private static final char ZERO = '0';

    private CharacterUtils() {}

    public static int parseInt(char c) {
        return c - ZERO;
    }
}
