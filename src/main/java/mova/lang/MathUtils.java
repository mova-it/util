package mova.lang;

import java.util.stream.IntStream;

public class MathUtils {

    public static final String PLUS = "+";
    public static final String MINUS = "-";

    private MathUtils() {}

    public static double round(double d, int decimalPlace) {
        double exp = tenPow(decimalPlace);
        return Math.round(d*exp)/exp;
    }

    public static float round(float d, int decimalPlace) {
        float exp = tenPow(decimalPlace);
        return Math.round(d*exp)/exp;
    }

    public static int tenPow(int pow) {
        int exp = 1;
        for (int i = 0; i < pow; i++) exp *= 10;

        return exp;
    }

    public static int euclidianModulo(int a, int mod) {
        return a >= 0 ? a%mod : (a%mod + mod)%mod;
    }

    public static String getSign(int number) {
        return Integer.signum(number) == -1 ? MINUS : PLUS;
    }

    public static String toSignedString(int number) {
        return getSign(number) + Math.abs(number);
    }

    public static int ceil(float f) {
        return (int) (f > 0 ? f + 1 : f);
    }

    public static boolean isNotPowerOfTwo(int n) {
        return (n & (n - 1)) != 0;
    }

    public static int countbits(int n) {
        int count = 0;
        while (n > 0) {
            count += (n & 1);
            n >>= 1;
        }
        return count;
    }

    public static boolean isBetween(double x, double b1, double b2) {
        if (b1 > b2) {
            double tmp = b1;
            b1 = b2;
            b2 = tmp;
        }

        return x >= b1 && x <= b2;
    }

    public static double[] toDoubles(float[] floats) {
        return IntStream.range(0, floats.length).mapToDouble(i -> floats[i]).toArray();
    }
}
