package mova.lang;

public class ExceptionUtils {

    private ExceptionUtils() {}

    public static String getStackTrace(Throwable throwable) {
        return org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(throwable);
    }
}
