package mova.lang;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SteppedThread extends Thread {

    private static final Logger LOGGER = Logger.getLogger(SteppedThread.class.getName());

    public SteppedThread(Runnable target) {
        super(target);
    }

    @Override
    public final void run() {
        init();

        super.run();

        release();
    }

    public void init() {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("Initialization by " + Thread.currentThread().getName());
        }
    }

    public void release() {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("Release by" + Thread.currentThread().getName());
        }
    }
}
