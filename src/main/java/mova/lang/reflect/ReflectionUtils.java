package mova.lang.reflect;

import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionUtils {

    private ReflectionUtils() {}

    public static Method[] getMethods(Class<?> clazz, String name) {
        return Arrays.stream(clazz.getMethods()).filter(method -> method.getName().equals(name)).toArray(Method[]::new);
    }
}
