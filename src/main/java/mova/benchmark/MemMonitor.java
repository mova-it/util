package mova.benchmark;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MemMonitor {

    enum MemSpace {
        EDEN("Eden Space"), SURVIVOR("Survivor Space");

        public final String spaceName;

        MemSpace(String spaceName) {
            this.spaceName = spaceName;
        }
    }

    private static final String[] BYTE_LABELS = {" b", " kb", " Mb", " Gb"};
    public static final int BYTES_PER_LEVEL = 1024;
    public static final int NANOS_IN_SECONDS = 1000000000;
    private final long startTime = System.nanoTime();

    private final Data heapSizeStats = new Data();
    private final Data allocSizeStats = new Data();
    private final Data allocIncPerUpdateStats = new Data();

    private int numHeapIncs = 0;

    // Il faudrait sérieusement voir a ce que ce soit static. On ne construit pas un MemMonitor. tout au plus on le démarre et on lui demande les infos.
    public void takeSample() {

        Runtime runtime = Runtime.getRuntime();
        long heapMemory = runtime.totalMemory();
        long allocatedMemory = heapMemory - runtime.freeMemory();

        if (heapMemory > heapSizeStats.lastValue) numHeapIncs++;
        if (allocatedMemory >= allocSizeStats.lastValue) {
            allocIncPerUpdateStats.accept(allocatedMemory - allocSizeStats.lastValue);
        }

        heapSizeStats.accept(heapMemory);
        allocSizeStats.accept(allocatedMemory);
    }

    private static String toByteFormat(double numBytes) {
        int labelIndex = 0;

        while (labelIndex < BYTE_LABELS.length - 1 && numBytes > BYTES_PER_LEVEL) {
            numBytes /= BYTES_PER_LEVEL;
            labelIndex++;
        }

        return Math.round(numBytes * 10) / 10f + BYTE_LABELS[labelIndex];
    }

    public String getReport() {
        long reportTime = System.nanoTime();
        long executionTime = reportTime - startTime;
        float executionTimeInSecs = (float) executionTime / NANOS_IN_SECONDS;

        StringBuilder builder = new StringBuilder();

        builder.append("Heap size: ").append(heapSizeStats).append("\n")
                .append("Allocation: ").append(allocSizeStats).append("\n")
                .append("Allocation inc/update: ").append(allocIncPerUpdateStats).append("\n")
                .append("Heap incs: ").append(numHeapIncs).append("\n")
                .append("\tTotal Time: ").append(executionTimeInSecs).append("s; Nb Samples: ").append(heapSizeStats.getCount()).append("\n");




        // Optimisation pour le min heap size
        long configuredMinHeapSize = getConfiguredMinHeapSize();
        if (configuredMinHeapSize < heapSizeStats.getMax()) {
            builder.append("Vous pourriez augmenter la taille minimal de votre heapmemory pour éviter les \"starting GCs\"");
            builder.append("\n\t- current minimum heap size = ").append(toByteFormat(configuredMinHeapSize));
            builder.append("\n\t- advised minimum heap size >= ").append(toByteFormat(heapSizeStats.getMax()));
        }


        int desiredFPS = 60; // en frame par seconde
        int desiredGCPeriod = 10; // En secondes

        // Optimisation pour le min young gen size
        // On souhaite que l'espace soit suffisemment grand pour qu'il ne soit nettoyer que toutes les X secondes pour un FPS donné
        double avgAllocationPerUpdate = allocIncPerUpdateStats.getAverage();
        double estimateYoungGenSize = avgAllocationPerUpdate * desiredFPS * desiredGCPeriod;
        long configuredMinYoungGenSize = getConfiguredMinYoungGenSize();
        if (configuredMinYoungGenSize < estimateYoungGenSize) {
            if (builder.length() > 0) builder.append("\n");
            builder.append("Vous pourriez augmenter la taille minimal du young gen space pour qu'il soit nettoyer moins fréquemment");
            builder.append("\n\t- current minimum young gen size = ").append(toByteFormat(configuredMinYoungGenSize));
            builder.append("\n\t- advised minimum young gen size >= ").append(toByteFormat(estimateYoungGenSize));
        } else if (2*configuredMinYoungGenSize > estimateYoungGenSize) {
            if (builder.length() > 0) builder.append("\n");
            builder.append("Vous pourriez diminuer la taille minimal du young gen space pour que le nettoyage soit moins long");
            builder.append("\n\t- current minimum young gen size = ").append(toByteFormat(configuredMinYoungGenSize));
            builder.append("\n\t- advised minimum young gen size >= ").append(toByteFormat(estimateYoungGenSize));
        }

        GCStats stats = GCStats.compute();
        double desiredGCFrequency = 1f/desiredGCPeriod;
        double currentGCFrequency = (double) stats.getGlobalGCCount()/executionTimeInSecs;

        if (currentGCFrequency > desiredGCFrequency) {
            if (builder.length() > 0) builder.append("\n");
            builder.append("Vous devriez augmenter la taille de déclenchement du GC pour tendre vers les valeurs indiquées");
            builder.append("\n\t- fréquence désirée = ").append(desiredGCFrequency).append(" GC/s");
            builder.append("\n\t- fréquence actuelle ").append(currentGCFrequency).append(" GC/s");
        }

        return builder.toString();
    }

    private static long getConfiguredMinHeapSize() {
        return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getInit();
    }

    private static long getConfiguredMinYoungGenSize() {
        Map<String, MemoryPoolMXBean> beans = ManagementFactory.getMemoryPoolMXBeans().stream()
                .filter(memoryPoolMXBean -> memoryPoolMXBean.getType() == MemoryType.HEAP)
                .filter(memoryPoolMXBean -> memoryPoolMXBean.getName().endsWith(MemSpace.EDEN.spaceName) || memoryPoolMXBean.getName().endsWith(MemSpace.SURVIVOR.spaceName))
                .collect(Collectors.toMap(memoryPoolMXBean1 -> memoryPoolMXBean1.getName().endsWith(MemSpace.EDEN.spaceName) ? MemSpace.EDEN.spaceName : MemSpace.SURVIVOR.spaceName, Function.identity()));

        long edenInitSpace = beans.get(MemSpace.EDEN.spaceName).getUsage().getInit();
        long survivorInitSpace = beans.get(MemSpace.SURVIVOR.spaceName).getUsage().getInit();

        return edenInitSpace + 2*survivorInitSpace;
    }

    private static class Data extends LongSummaryStatistics {

        private long lastValue = -1;

        @Override
        public void accept(long value) {
            lastValue = value;

            super.accept(value);
        }

        @Override
        public void combine(LongSummaryStatistics other) {
            throw new UnsupportedOperationException();
        }

        @Override
        public String toString() {
            return String.format(
                    "min=%s, max=%s, average=%s",
                    toByteFormat(getMin()),
                    toByteFormat(getMax()),
                    toByteFormat(getAverage()));
        }
    }
}
