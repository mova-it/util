package mova.benchmark;

class BenchmarkException extends RuntimeException {
    BenchmarkException(Exception e) {
        super("Une erreur s'est produite durant l'exécution de la fonction testée", e);
    }
}
