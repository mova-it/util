package mova.benchmark;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class GCStats {

    public enum GCType {
        MINOR, MAJOR
    }

    public enum GCProcess {
        PARALLEL_SCAVENGE("PS Scavenge", "PS MarkSweep"),
        CONCURRENT_MARK_SWEEP("ParNew", "ConcurrentMarkSweep");

        private final String[] names = new String[2];

        GCProcess(String minorGCName, String majorGCName) {
            this.names[GCType.MAJOR.ordinal()] = majorGCName;
            this.names[GCType.MINOR.ordinal()] = minorGCName;
        }

        String getName(GCType type) {
            return names[type.ordinal()];
        }
    }

    private final GCStat gcStats = new GCStat("Global");
    private final Map<String, GCStat> gcStatsPerGCType = new HashMap<>();

    private GCStats() {}

    public static GCStats compute() {
        GCStats stats = new GCStats();

        ManagementFactory.getGarbageCollectorMXBeans().forEach(stats::analyze);

        return stats;
    }

    private void analyze(GarbageCollectorMXBean gc) {
        long collectionCount = gc.getCollectionCount();
        long time = gc.getCollectionTime();

        // Si un des 2 vaut 0 ou -1, on ignore et on mais un log
        if (collectionCount <= 0 || time <= 0) {
            Logger.getLogger(getClass().getName()).warning(() -> gc.getName() + " show that " + collectionCount + " GCs take " + time);
            return;
        }

        gcStats.accept(collectionCount, time);
        gcStatsPerGCType.computeIfAbsent(gc.getName(), GCStat::new).accept(collectionCount, time);
    }

    public long getGlobalGCCount() {
        return gcStats.getCount();
    }

    public long getGlobalGCTotalTime() {
        return gcStats.getTotalTime();
    }

    public double getGlobalGCAverageTime() {
        return gcStats.getAverageTime();
    }

    public long getGCCount(GCProcess process, GCType type) {
        GCStat stat = gcStatsPerGCType.get(process.getName(type));
        return stat != null ? stat.getCount() : -1;
    }

    public long getGCTotalTime(GCProcess process, GCType type) {
        GCStat stat = gcStatsPerGCType.get(process.getName(type));
        return stat != null ? stat.getTotalTime() : -1;
    }

    public double getGCAverageTime(GCProcess process, GCType type) {
        GCStat stat = gcStatsPerGCType.get(process.getName(type));
        return stat != null ? stat.getAverageTime() : -1;
    }

    private static class GCStat {
        private final String name;
        private long count = 0;
        private long time = 0;
        private double avgTime = 0;

        private GCStat(String name) {
            this.name = name;
        }

        private void accept(long count, long time) {
            this.count += count;
            this.time += time;

            computeSecondaryStats();
        }

        public String getName() {
            return name;
        }

        public long getCount() {
            return count;
        }

        public long getTotalTime() {
            return time;
        }

        public double getAverageTime() {
            return avgTime;
        }

        private void computeSecondaryStats() {
            this.avgTime = (double) this.time /this.count;
        }
    }
}
