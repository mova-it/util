package mova.benchmark;

import java.util.Comparator;
import java.util.LongSummaryStatistics;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.LongStream;

public class Benchmark {

  private static final float ABERATIONS_PERCENTAGE = .05f;

  private static int defaultIterations = 10000;
  private static TimeUnit defaultTimeUnit = TimeUnit.MILLISECONDS;

  private Benchmark() {}

  public static void setDefaultIterations(int defaultIterations) {
    Benchmark.defaultIterations = defaultIterations;
  }

  public static void setDefaultTimeUnit(TimeUnit defaultTimeUnit) {
    Benchmark.defaultTimeUnit = defaultTimeUnit;
  }

  public static void of(Runnable function) {
    of(function, defaultTimeUnit);
  }

  public static void of(Runnable function, TimeUnit timeUnit) {
    of(function, defaultIterations, timeUnit);
  }

  public static void of(Runnable function, int iteration) {
    of(function, iteration, defaultTimeUnit);
  }

  public static void of(Runnable function, int iteration, TimeUnit timeUnit) {
    Logger logger = Logger.getLogger(Benchmark.class.getName());
    try {
      long initialTime = benchmark(function, timeUnit);
      logger.info(() -> "Initial time: " + initialTime + " " + timeUnit);

      LongStream stream = LongStream.generate(() -> benchmark(function, timeUnit)).limit(iteration);
      stream = stream.sorted().skip((long) (iteration * ABERATIONS_PERCENTAGE)); // On retire les abérations les plus petites
      stream = stream.boxed().sorted(Comparator.reverseOrder()).skip((long) (iteration * ABERATIONS_PERCENTAGE)).mapToLong(value -> value); // On supprime les abérations plus grandes

      LongSummaryStatistics stats = stream.summaryStatistics();
      logger.info(() -> stats + " [time-unit = " + timeUnit + "]");
    } catch (Exception e) {
      logger.log(Level.SEVERE, e, () -> "Un erreur s'est produite pendant le benchmark");
    }
  }

  private static long benchmark(Runnable function, TimeUnit timeUnit) {
    try {
      long t1 = System.nanoTime();

      function.run();

      long t2 = System.nanoTime();

      return timeUnit.convert(t2 - t1, TimeUnit.NANOSECONDS);
    } catch (Exception e) {
      throw new BenchmarkException(e);
    }
  }

}
