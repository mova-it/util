package mova.nio;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NioUtils {

    private static final String TMP_DIR = System.getProperty("java.io.tmpdir");

    private NioUtils() {}

    public static void removeTempFile(String filename) {
        File file = new File(TMP_DIR, filename);
        verboselyRemoveFile(file);
    }

    public static void verboselyRemoveFile(File file) {
        if (file != null && file.exists()) {
            try {
                Files.delete(file.toPath());
            } catch (IOException e) {
                Logger.getLogger(NioUtils.class.getName()).log(Level.SEVERE, e, () -> "Impossible de supprimer le fichier suivant: " + file.getAbsolutePath());
            }
        }
    }

    public static byte[] getFileData(File file) throws IOException {
        return getFileInputStreamData(new FileInputStream(file));
    }

    public static byte[] getInputStreamData(InputStream inputStream) throws IOException {
        if (inputStream instanceof FileInputStream) return getFileInputStreamData((FileInputStream) inputStream);

        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)) {
            return IOUtils.toByteArray(bufferedInputStream);
        }
    }

    public static byte[] getFileInputStreamData(FileInputStream fileInputStream) throws IOException {
        try (FileInputStream fileInputStream0 = fileInputStream) {
            FileChannel fc = fileInputStream0.getChannel();
            long size = fc.size();
            ByteBuffer bBuff = ByteBuffer.allocate((int) size);
            //Démarrage de la lecture
            fc.read(bBuff);
            //On prépare à la lecture avec l'appel à flip
            bBuff.flip();
            return bBuff.array();
        }
    }

    public static int writeData(InputStream inputStream, File target) throws IOException {
        return writeData(inputStream, new FileOutputStream(target), false);
    }

    public static int writeData(byte[] byteArray, File target) throws IOException {
        return writeData(byteArray, new FileOutputStream(target));
    }

    public static int writeData(ByteBuffer byteBuffer, File target) throws IOException {
        return writeData(byteBuffer, new FileOutputStream(target));
    }

    public static int writeData(ByteArrayOutputStream byteArrayOutputStream, FileOutputStream fileOutputStream) throws IOException {
        return writeData(byteArrayOutputStream.toByteArray(), fileOutputStream);
    }

    public static int writeData(byte[] byteArray, FileOutputStream fileOutputStream) throws IOException {
        return writeData(ByteBuffer.wrap(byteArray), fileOutputStream);
    }

    public static int writeData(InputStream inputStream, FileOutputStream fileOutputStream, boolean fsyncEnabled) throws IOException {
        byte[] byteArray;
        if (inputStream instanceof FileInputStream) {
            byteArray = getFileInputStreamData((FileInputStream) inputStream);
        } else {
            byteArray = IOUtils.toByteArray(inputStream);
        }
        return writeData(byteArray, fileOutputStream, fsyncEnabled);
    }

    public static int writeData(byte[] byteArray, FileOutputStream fileOutputStream, boolean fsyncEnabled) throws IOException {
        return writeData(ByteBuffer.wrap(byteArray), fileOutputStream, fsyncEnabled);
    }

    public static int writeData(ByteBuffer byteBuffer, FileOutputStream fileOutputStream) throws IOException {
        return writeData(byteBuffer, fileOutputStream, false);
    }

    public static int writeData(ByteBuffer byteBuffer, FileOutputStream fileOutputStream, boolean fsyncEnabled) throws IOException {
        try (FileOutputStream fileOutputStream0 = fileOutputStream) {
            FileChannel fc = fileOutputStream0.getChannel();
            int bytesWriten = fc.write(byteBuffer);

            if (fsyncEnabled) fileOutputStream0.getFD().sync();

            return bytesWriten;
        }
    }

}
