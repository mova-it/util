package mova.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayUtilsTest {

    @Test
    void printSquareFormat_square() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int w = 3;
        int h = 3;
        int intLength = 1;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[ 1 2 3 ]\n" +
                "[ 4 5 6 ]\n" +
                "[ 7 8 9 ]", result);
    }

    @Test
    void printSquareFormat_widerThanHigh() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int w = 4;
        int h = 3;
        int intLength = 1;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[ 1 2 3 4 ]\n" +
                "[ 5 6 7 8 ]\n" +
                "[ 9101112 ]", result);
    }

    @Test
    void printSquareFormat_widerThanHigh_2spaces() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int w = 4;
        int h = 3;
        int intLength = 2;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[  1  2  3  4 ]\n" +
                "[  5  6  7  8 ]\n" +
                "[  9 10 11 12 ]", result);
    }

    @Test
    void printSquareFormat_higherThanWide() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int w = 3;
        int h = 4;
        int intLength = 1;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[ 1 2 3 ]\n" +
                "[ 4 5 6 ]\n" +
                "[ 7 8 9 ]\n" +
                "[101112 ]", result);
    }

    @Test
    void printSquareFormat_higherThanWide_2spaces() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int w = 3;
        int h = 4;
        int intLength = 2;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[  1  2  3 ]\n" +
                "[  4  5  6 ]\n" +
                "[  7  8  9 ]\n" +
                "[ 10 11 12 ]", result);
    }

    @Test
    void printSquareFormat_notFull() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7, 8};
        int w = 3;
        int h = 3;
        int intLength = 1;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[ 1 2 3 ]\n" +
                "[ 4 5 6 ]\n" +
                "[ 7 8   ]", result);
    }

    @Test
    void printSquareFormat_tooMany() {
        int[] ints = new int[] {1, 2, 3, 4, 5, 6, 7};
        int w = 4;
        int h = 4;
        int intLength = 1;

        String result = ArrayUtils.printSquareFormat(ints, w, h, intLength);

        assertEquals("[ 1 2 3 4 ]\n" +
                "[ 5 6 7   ]\n" +
                "[         ]\n" +
                "[         ]", result);
    }
}
