package mova.util.function;

import mova.util.RandomUtils;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

class FunctionalInterfaceUtilsTest {

    @Test
    void create_null_consumer() {
        assertThrows(NullPointerException.class, () -> FunctionalInterfaceUtils.onlyOnce((Consumer<?>) null));
    }

    @Test
    void create_null_runnable() {
        assertThrows(NullPointerException.class, () -> FunctionalInterfaceUtils.onlyOnce((Runnable) null));
    }

    @Test
    void andThen_null_consumer() {
        Consumer<?> callback = FunctionalInterfaceUtils.onlyOnce(FunctionalInterfaceUtils.noopConsumer());
        assertThrows(NullPointerException.class, () -> {
            //noinspection DataFlowIssue
            callback.andThen(null);
        });
    }

    @Test
    void acceptOnce() {
        AtomicInteger callbackCounter = new AtomicInteger();
        Consumer<?> callback = FunctionalInterfaceUtils.onlyOnce(ignore -> callbackCounter.incrementAndGet());

        assertEquals(0, callbackCounter.get());
        assertFalse(((Doable) callback).isDone());

        callback.accept(null);
        assertEquals(1, callbackCounter.get());
        assertTrue(((Doable) callback).isDone());

        for (int j = 0; j < RandomUtils.randInt(200) + 1; j++) {
            callback.accept(null);
            assertEquals(1, callbackCounter.get());
            assertTrue(((Doable) callback).isDone());
        }
    }

    @Test
    void runOnce() {
        AtomicInteger callbackCounter = new AtomicInteger();
        Runnable callback = FunctionalInterfaceUtils.onlyOnce(callbackCounter::incrementAndGet);

        assertEquals(0, callbackCounter.get());
        assertFalse(((Doable) callback).isDone());

        callback.run();
        assertEquals(1, callbackCounter.get());
        assertTrue(((Doable) callback).isDone());

        for (int j = 0; j < RandomUtils.randInt(200) + 1; j++) {
            callback.run();
            assertEquals(1, callbackCounter.get());
            assertTrue(((Doable) callback).isDone());
        }
    }

    @Test
    void acceptOnceAndThen() {
        AtomicInteger callbackCounter = new AtomicInteger();
        AtomicInteger andThenCounter = new AtomicInteger();
        Consumer<?> callback = FunctionalInterfaceUtils.onlyOnce(ignore -> callbackCounter.incrementAndGet());
        Consumer<?> andThenCallback = callback.andThen(ignore -> andThenCounter.incrementAndGet());

        assertEquals(0, callbackCounter.get());
        assertEquals(0, andThenCounter.get());
        assertFalse(((Doable) callback).isDone()); // Le callback initial est écrasé, donc jamais appelé
        assertFalse(((Doable) andThenCallback).isDone());

        andThenCallback.accept(null);
        assertEquals(1, callbackCounter.get());
        assertEquals(1, andThenCounter.get());
        assertFalse(((Doable) callback).isDone()); // Le callback initial est écrasé, donc jamais appelé
        assertTrue(((Doable) andThenCallback).isDone());

        for (int j = 0; j < RandomUtils.randInt(200) + 1; j++) {
            andThenCallback.accept(null);
            assertEquals(1, callbackCounter.get());
            assertEquals(1, andThenCounter.get());
            assertFalse(((Doable) callback).isDone()); // Le callback initial est écrasé, donc jamais appelé
            assertTrue(((Doable) andThenCallback).isDone());
        }
    }
}
