package mova.lang;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathUtilsTest {

    private static final double DOUBLE_RANGE = 1E4;

    @Test
    void isBetweenTest() {
        double rd = DOUBLE_RANGE/2 - Math.random()*DOUBLE_RANGE;
        double b = Math.abs(rd) + Math.random();

        assertTrue(MathUtils.isBetween(rd, b, -b));
        assertTrue(MathUtils.isBetween(rd, -b, b));
        assertFalse(MathUtils.isBetween(rd, b, b));
        assertFalse(MathUtils.isBetween(rd, -b, -b));
        assertFalse(MathUtils.isBetween(-rd, b, b));
    }


}
